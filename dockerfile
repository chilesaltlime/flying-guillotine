FROM python:3.7-slim

COPY requirements.txt .
RUN pip install -r requirements.txt
RUN pip install pyyaml
RUN apt update 
RUN apt install libffi-dev libcairo2 -y
RUN mkdir /opt/fg
COPY . /opt/fg/
WORKDIR /opt/fg/
CMD python bot.py
