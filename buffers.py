import pandas as pd
import discord
import asyncio
from discord.utils import get as discord_get

import time
from lib import img_commands#, relative_guillotine#, leaderboard

 
from logger_default import custom_logger
logger = custom_logger(__name__).logger

class BaseBuffer:
    def __init__(self, config, role_name):
        self.df = pd.DataFrame()
        # can abstract this to a loop but may be confusing
        # for others
        self.update_config(config)
        self.role_name = role_name
        self.users_with_role = []
        self.admin_users = []
        
    def update_config(self, config):
        self.duration = config["duration"] if "duration" in config else 30
        self.min_votes = config['min_voters']
        self.timeout = config["timeout"] if "timeout" in config else 30
        self.vote_emoji = config["vote_emoji"] if "vote_emoji" in config else None
        self.can_vote = config["can_vote"] if "can_vote" in config else []
        self.cant_vote = config["cant_vote"] if "cant_vote" in config else []
        self.can_receive = config["can_receive"] if "can_receive" in config else []
        self.img_command = config["img_command"] if "img_command" in config else ""

    async def check_validity(self, payload, channel):
        logger.info(payload.emoji)

        message = await channel.fetch_message(payload.message_id)
        logger.info("ORIGINAL MESSAGE")
        logger.info(message.content)
      
        logger.info('MEMBER')
        logger.info(set(self.can_vote) & set(
            [r.name for r in payload.member.roles]))
        has_role = set(self.can_vote) & set(
            [r.name for r in payload.member.roles])
        # has_forbidden_role = set(self.cant_vote) & set(
        #     [r.name for r in payload.member.roles])

        logger.info(f"HAS ROLE {has_role}")
        # logger.info(f"HAS_FORBIDDEN_ROLE {has_forbidden_role}")

        has_target_role = message.author in self.users_with_role
        logger.info(f"{self.role_name.upper()} USERS {self.users_with_role}")
        logger.info(f"MESSAGE AUTHOR {message.author}")
        # can_be_target = set(self.can_receive) & set(
        #     [r.name for r in message.author.roles])

        self_vote = message.author == payload.member
        # logger.info(f"CAN BE TARGET {can_be_target}")

        if has_role and not has_target_role and not self_vote:
            logger.info(f"USER {payload.member} VOTED AGAINST {message.author}")
            await self.append({'reactor': payload.member,
                                'reactor_str': str(payload.member),
                                'message_author_str': str(message.author.name),
                                'message_author': message.author,
                                'time': time.time(), 'channel': channel}, guild_id=payload.guild_id)

        else:
            logger.info("USER NOT AUTHORIZED TO VOTE")    

    async def append(self, row, guild_id):
        logger.info('APPEND')
        self.df = self.df.append(row, ignore_index=True)
        self.prune_time()
        await self.analyze(guild_id)

    def prune_time(self):
        logger.info('PRUNE')
        now = int(time.time())

        self.df = self.df[(now - self.df['time']) < self.timeout]

    def get_unique_reacts(self):
        return self.df[['message_author_str', 'reactor']].groupby(
                'message_author_str').nunique()['reactor']

    async def analyze(self, guild_id):
        logger.info('DATAFRAME')
        logger.info(self.df)
        logger.info('count unique')

        try:
            unique_reacts = self.get_unique_reacts()
            logger.info('unique reacts')
            logger.info(unique_reacts)

            

        except TypeError:
            self.df.to_csv('debug.csv')
            raise

        logger.info(f'{self.role_name.upper()} STR LIST')


        consequence_list_str = unique_reacts[unique_reacts >=
                                             self.min_votes].index.values

        logger.info(consequence_list_str)

        consequence_list = []
        for author in consequence_list_str:
            consequence_list.append(
                self.df[self.df['message_author_str'] == author]['message_author'].values[0])

        logger.info(f'{self.role_name.upper()} LIST')        
        if consequence_list:
            await self.iter_consequence(consequence_list)

    async def iter_consequence(self, consequence_list):
        logger.info(f'ITER {self.role_name.upper()}')
        for member in consequence_list:

            ctx = self.df[self.df['message_author'] == member].tail(1)[
                'channel'].values[0]

            logger.info('CHANNEL')
            logger.info(ctx)
           
            await self.face_the_consequences(ctx, member)
            logger.info(f'member {member}')
           
    async def face_the_consequences(self, ctx, target_user: discord.User):

        try:
            assert target_user not in self.users_with_role
        except Exception as e:
            logger.warning(f"*{target_user} is already {self.role_name}d!*")
            return 

        self.admin_role = discord_get(ctx.guild.roles, name='admin')
        
        if self.admin_role is not None and self.admin_role in target_user.roles:
            self.admin_users.append(target_user.id)
            await target_user.remove_roles(self.admin_role)


        target_role = None
        try:
            self.users_with_role.append(target_user)
            
            target_role = await self.create_target_role(ctx, target_user)
            asyncio.create_task(
                self.suspend_and_return(ctx, target_user, target_role)
            )
            self.increment_execution(ctx, target_user)

        except Exception as e:
            logger.info(e)
            await ctx.send(f"*{target_user} can't be {self.role_name}d, they are simply too powerful!*")
            await self.clean_up(ctx, target_role, target_user)


    async def suspend_and_return(self, ctx, target_user, target_role):
        # Give role to member

        try:
            await ctx.guild.get_member(target_user.id).add_roles(target_role)
            # await img_commands.send_img(ctx.guild.id, self.img_command, ctx.send, None, f"**{target_user.mention}: You have been {self.role_name}d for at least {self.duration} seconds.**")
            await ctx.send(f"**{target_user.mention}: You have been {self.role_name}d for at least {self.duration} seconds.**")

            await asyncio.sleep(self.duration)
        except Exception as e:
            logger.warning('ERROR')
            logger.info(e)
            
            logger.info(e.args)            
            
        finally:
            # Remove from muted_users            
            await self.clean_up(ctx, target_role, target_user)                        
            await ctx.send("**{0} is back from exile.**".format(target_user.mention))
            

    async def clean_up(self, ctx, target_role, target_user):
        logger.info('CLEANING BUFFER')                    
        if 'message_author_str' in self.df.columns:                
                logger.info(target_user)                
                logger.info(self.df['message_author'])
                self.df = self.df[self.df['message_author'] != target_user]
        if target_user in self.users_with_role:
                self.users_with_role.remove(target_user)
        if target_role in target_user.roles:
            await target_user.remove_roles(target_role)
        if target_user.id in self.admin_users:
            logger.info("RESTORING ADMIN")
            await target_user.add_roles(self.admin_role)
            self.admin_users.remove(target_user.id)


    async def create_target_role(self, ctx, target_user):
        pass


class MuteBuffer(BaseBuffer):

    def increment_execution(self, ctx, target_user):
        # leaderboard.inc_mute_execution(ctx.guild.id, target_user.id)
        pass

    async def create_target_role(self, ctx, target_user):
        '''create a role that stops people from sending messages'''
        # use existing guillotine role if exists
        muted_role = None
        for role in ctx.guild.roles:
            if role.name=="guillotined":
                muted_role = role
                

        if muted_role is None:
            muted_role = await ctx.guild.create_role(name="guillotined")
        

        new_pos = max(ctx.guild.get_member(target_user.id).top_role.position+1, muted_role.position)
        await muted_role.edit(position=new_pos)
        # change channel permissions for new role
        # for ch in ctx.guild.channels:
        #     if type(ch) is discord.VoiceChannel:
        #         await ch.set_permissions(muted_role, connect=False)

        return muted_role

    async def clean_up(self, ctx, target_role, target_user):
        await super().clean_up(ctx, target_role, target_user)
        # try:
        #     await target_role.delete()
        # except:
        #     logger.info("can't delete role!")
        # leftovers =  [x for x in ctx.guild.roles if x.name == 'guillotined']

        # if not self.users_with_role:
        #     for leftover_role in leftovers:
        #         try: 
        #             await leftover_role.delete()
        #         except discord.errors.NotFound:
        #             pass
    

class GulagBuffer(BaseBuffer):

    def increment_execution(self, ctx, target_user):
        # leaderboard.inc_gulag_execution(ctx.guild.id, target_user.id)
        pass

    async def create_target_role(self, ctx, target_user):

        # muted_role = await ctx.guild.create_role(name="gulag", color='c41c1c')

        self.poster = [
            x for x in ctx.guild.roles if x.name == 'posterteriat'][0]
        gulag = [x for x in ctx.guild.roles if x.name == 'bonk'][0]


        await target_user.remove_roles(self.poster)

        assert self.poster not in target_user.roles

        if target_user.guild_permissions.send_messages:
            raise ValueError('User cannot be gulaged')

        return gulag

    async def clean_up(self, ctx, target_role, target_user):
        await super().clean_up(ctx, target_role, target_user)
        await target_user.add_roles(self.poster)



class DelBuffer:
    def __init__(self, timeout, votes):
        self.df = pd.DataFrame(
            columns=['user', 'message', 'message_id', 'time', 'channel'])
        self.timeout = timeout
        self.DEL_VOTERS = votes

    def update_config(self, timeout, votes):
        self.timeout = timeout
        self.DEL_VOTERS = votes

    def increment_execution(self, guild_id, user_id):
        # leaderboard.inc_del_execution(guild_id, user_id)
        pass

    async def append(self, row):
        logger.info('APPEND')

        self.df = self.df.append(row, ignore_index=True)
        self.prune_time()
        await self.analyze()

    def prune_time(self):
        logger.info('PRUNE')
        now = int(time.time())
        self.df = self.df[(now-self.df['time']) < self.timeout]

    async def analyze(self):
        logger.info('DATAFRAME')
        logger.info(self.df)

        gb = self.df.groupby('message_id').count()['time']

        logger.info(gb)
        to_delete = gb[gb >= self.DEL_VOTERS].index
        logger.info(f"TO DELETE {to_delete}")

        for message_id in to_delete:
            message = self.df[self.df['message_id']
                              == message_id]['message'].values[0]
            logger.info(message)
            logger.info("DELETE MESSAGE {message}")
            command = 'bee'
            self.increment_execution(message.guild.id, message.author.id)
            await message.delete()
            await img_commands.send_img(message.guild.id, command, message.channel.send)

            # await reacc(message.channel, 'bee')
            self.df = pd.DataFrame(columns=['user', 'message', 'message_id', 'time', 'channel'])

