def svg_png(emoji_path='crown.svg'):
        import cairosvg

        print('emoji_path')
        temp = emoji_path.replace('.svg', '.png')
        print(emoji_path)
        print(temp)
        cairosvg.svg2png(url=emoji_path, write_to=temp,
                         output_width=256, output_height=512
                         )
        emoji_path = temp
        return emoji_path


def overlay_canvas():
    from PIL import Image, ImageEnhance, ImageOps, ImageSequence

    cr = Image.open('crown.png')

    canv = 512
    scale = 384
    canvas = Image.new('RGBA', (canv, canv))

    cr = cr.resize((scale,scale))

    canvas.paste(cr, (int((canv-scale)/2),-94), cr)

    canvas.save('crown.png')


if __name__ == '__main__':

    svg_png()
    overlay_canvas()