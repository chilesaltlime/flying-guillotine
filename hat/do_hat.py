from PIL import Image

import numpy as np

import cairosvg





def overlay_hat(emoji_path,  overlay='cowboy', base_size=512, size_ratio=1):

    res = (base_size, base_size)

    if overlay in ['halo', 'cowboy', 'tinfoil','crown', 'comrade']:
        size_ratio = 1.25

    if overlay in ['vape']:
        size_ratio = 1

    scale = int(base_size // size_ratio)
    def svg_png(emoji_path):

        print('emoji_path')
        temp = emoji_path.replace('.svg', '.png')
        print(emoji_path)
        print(temp)
        cairosvg.svg2png(url=emoji_path, write_to=temp,
                         output_width=scale, output_height=scale
                         )
        emoji_path = temp
        return emoji_path

    if emoji_path.endswith('svg'):
        emoji_path = svg_png(emoji_path)

    hat = Image.open(f'hat/{overlay}.png')

    target = Image.open(emoji_path).resize((scale, scale))

    hat = hat.resize(res)

    print(target.size)

    canvass = Image.new(target.mode, res)
    if overlay in ['cowboy', 'halo', 'tinfoil']:

        center = (int((base_size - scale) // 2), int((base_size - scale) // 1))
        print(center)
        canvass.paste(target, center)

        canvass.paste(hat, (0, -int(base_size*0.025)), hat)
    else:
        center = (int((base_size - scale) // 2), int((base_size - scale) // 1))
        print(str(center))
        print("DRAWING OVERLAY")
        print(overlay)

        canvass.paste(target, center)

        canvass.paste(hat, (0, 0), hat)

    outpath = emoji_path

    canvass = canvass.resize((64, 64))
    canvass.save(outpath)

    canvass.save('hat/target.png')
    return outpath


if __name__ == "__main__":
    pass

    overlay_hat('pogfish.png')
