"""

flying guillotine bot.


Inspired by Comrade Bot by turtlebasket
"""
from logger_default import custom_logger

logger = custom_logger(__name__).logger
import hashlib
from hat.randemoji import random_emoji
from pathlib import Path
import asyncio
import datetime
import yaml

import math
import random
import time
from os import environ
from urllib.request import Request, urlopen
import json
import discord
import dbl
import pandas as pd

# from bot_utils import *
from discord.ext import commands, tasks
from discord.utils import get

from hat.do_hat import overlay_hat
from fry.deepfry import deep_fry
from lib import (
    buffer_container,
    img_api,
    img_commands,
    leaderboard,
    # relative_guillotine,
)

with open("tokens.json", "r") as tokens_file:
    tokens = json.load(tokens_file)

STATUS_LOOP = 480

task_wrapper = {"task": None}
config = {}

"""TODO organize into cogs, wrap things into status loop or onstart """


def init_global_config():
    global config
    # config = db.get_global_config()
    config = yaml.safe_load(open("config.yaml", "r"))

    if config is not None:
        global STATUS_LOOP
        global LOG_LEVEL
        STATUS_LOOP = config["STATUS_LOOP"]
        LOG_LEVEL = config["LOG_LEVEL"]


init_global_config()
intents = discord.Intents.default()

intents.members = True

bot = commands.Bot(command_prefix=">>", intents=intents)
bot.remove_command("help")


async def status_loop():
    while True:
        await bot.change_presence(
            activity=discord.Game(name="God in {0} servers".format(len(bot.guilds)))
        )
        await asyncio.sleep(STATUS_LOOP)

        await bot.change_presence(activity=discord.Game(name=">>help"))
        await asyncio.sleep(STATUS_LOOP)

        await bot.change_presence(
            activity=discord.Game(name="Dictatorship of the postertariat")
        )
        await asyncio.sleep(STATUS_LOOP)


class TopGG(commands.Cog):
    """Handles interactions with the top.gg API"""

    def __init__(self, bot):
        self.bot = bot
        self.token = tokens["dbl_token"]
        # refresh guild count every 30 mins
        self.dblpy = dbl.DBLClient(self.bot, self.token, autopost=True)

        for guild in bot.guilds:
            # config = db.get_config(guild.id)
            guild_config = yaml.safe_load(open("config.yaml", "r"))[guild.id]

            buffer_container.init_buffer(guild.id, guild_config)
            # img_commands.init(guild.id)
            # relative_guillotine.init(guild.id, guild_config["MIN_VOTES"])

    @tasks.loop(minutes=30.0)
    async def update_stats(self):
        """Automatically update server count"""
        try:
            await self.dblpy.post_guild_count()
        except Exception as e:
            logger.info(
                "Failed to post server count\n{}: {}".format(type(e).__name__, e)
            )


@bot.listen("on_message")
async def start_poster(message):

    global poster_running

    try:
        # relative_guillotine.on_message(message)
        if not poster_running:
            logger.info(f"starting postertariat")
            poster_running = 1

            postertariat.start()
            guillotine_role_perms.start()

    except Exception as e:
        logger.info(e)


@bot.event
async def on_ready():
    task_wrapper["task"] = bot.loop.create_task(status_loop())
    bot.add_cog(TopGG(bot))

    logger.info("Bot started.")
    logger.info("--------------------------")


overlay_emojis = (
    "🤠",
    "😍",
    "🤩",
    "😎",
    "😇",
    "fedora",
    "pogvape",
    "ahe",
    "nuke",
    "coolkitten",
    "monarch",
    "ushunka",
)
overlay_text = (
    "cowboy",
    "hearts",
    "stars",
    "sunnies",
    "halo",
    "tinfoil",
    "pogman",
    "ahegao",
    "deepfry",
    "giffry",
    "crown",
    "comrade",
)
overlay_all = overlay_emojis + overlay_text
overlay_dict = dict(zip(overlay_emojis, overlay_text))


@bot.command(aliases=["OBAMNA"])
async def obamna(ctx):
    await ctx.send(file=discord.File("lib/obamna.mp3"))


@bot.command(aliases=["gigachad"])
async def chad(ctx):
    await ctx.message.delete()
    await ctx.send(
        f""" `
⠀⠀⠘⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡜⠀⠀⠀
⠀⠀⠀⠑⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡔⠁⠀⠀⠀
⠀⠀⠀⠀⠈⠢⢄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⠴⠊⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⢸⠀⠀⠀⢀⣀⣀⣀⣀⣀⡀⠤⠄⠒⠈⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠘⣀⠄⠊⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀
⣿⣿⣿⣿⣿⣿⣿⣿⡿⠿⠛⠛⠛⠋⠉⠈⠉⠉⠉⠉⠛⠻⢿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⡿⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠛⢿⣿⣿⣿⣿
⣿⣿⣿⣿⡏⣀⠀⠀⠀⠀⠀⠀⠀⣀⣤⣤⣤⣄⡀⠀⠀⠀⠀⠀⠀⠀⠙⢿⣿⣿
⣿⣿⣿⢏⣴⣿⣷⠀⠀⠀⠀⠀⢾⣿⣿⣿⣿⣿⣿⡆⠀⠀⠀⠀⠀⠀⠀⠈⣿⣿
⣿⣿⣟⣾⣿⡟⠁⠀⠀⠀⠀⠀⢀⣾⣿⣿⣿⣿⣿⣷⢢⠀⠀⠀⠀⠀⠀⠀⢸⣿
⣿⣿⣿⣿⣟⠀⡴⠄⠀⠀⠀⠀⠀⠀⠙⠻⣿⣿⣿⣿⣷⣄⠀⠀⠀⠀⠀⠀⠀⣿
⣿⣿⣿⠟⠻⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠶⢴⣿⣿⣿⣿⣿⣧⠀⠀⠀⠀⠀⠀⣿
⣿⣁⡀⠀⠀⢰⢠⣦⠀⠀⠀⠀⠀⠀⠀⠀⢀⣼⣿⣿⣿⣿⣿⡄⠀⣴⣶⣿⡄⣿
⣿⡋⠀⠀⠀⠎⢸⣿⡆⠀⠀⠀⠀⠀⠀⣴⣿⣿⣿⣿⣿⣿⣿⠗⢘⣿⣟⠛⠿⣼
⣿⣿⠋⢀⡌⢰⣿⡿⢿⡀⠀⠀⠀⠀⠀⠙⠿⣿⣿⣿⣿⣿⡇⠀⢸⣿⣿⣧⢀⣼
⣿⣿⣷⢻⠄⠘⠛⠋⠛⠃⠀⠀⠀⠀⠀⢿⣧⠈⠉⠙⠛⠋⠀⠀⠀⣿⣿⣿⣿⣿
⣿⣿⣧⠀⠈⢸⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠟⠀⠀⠀⠀⢀⢃⠀⠀⢸⣿⣿⣿⣿
⣿⣿⡿⠀⠴⢗⣠⣤⣴⡶⠶⠖⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⡸⠀⣿⣿⣿⣿
⣿⣿⣿⡀⢠⣾⣿⠏⠀⠠⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠛⠉⠀⣿⣿⣿⣿
⣿⣿⣿⣧⠈⢹⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣰⣿⣿⣿⣿
⣿⣿⣿⣿⡄⠈⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⣴⣾⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣧⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣷⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣦⣄⣀⣀⣀⣀⠀⠀⠀⠀⠘⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⡄⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⠀⠀⠀⠙⣿⣿⡟⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠇⠀⠁⠀⠀⠹⣿⠃⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⡿⠛⣿⣿⠀⠀⠀⠀⠀⠀⠀⠀⢐⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⠿⠛⠉⠉⠁⠀⢻⣿⡇⠀⠀⠀⠀⠀⠀⢀⠈⣿⣿⡿⠉⠛⠛⠛⠉⠉
⣿⡿⠋⠁⠀⠀⢀⣀⣠⡴⣸⣿⣇⡄⠀⠀⠀⠀⢀⡿⠄⠙⠛⠀⣀⣠⣤⣤⠄ `"""
    )


@bot.command(aliases=["kek"])
async def kekw(ctx):
    await ctx.send(
        f"""`⢰⣶⠶⢶⣶⣶⡶⢶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⡶⠶⢶⣶⣶⣶⣶
⠘⠄⠄⠄⠄⠄⠄⠄⠄⣿⣿⣿⣿⣿⣿⣿⠿⠄⠄⠄⠈⠉⠄⠄⣹⣶⣿⣿⣿⣿⢿
⠄⠤⣾⣿⣿⣿⣿⣷⣤⡈⠙⠛⣿⣿⣿⣧⣀⠠⣤⣤⣴⣶⣿⣿⣿⣿⣿⣿⣿⣿⣶
⢠⠄⠄⣀⣀⣀⣭⣿⣿⣿⣶⣿⣿⣿⣿⣿⣿⣤⣿⣿⠉⠉⠉⢉⣉⡉⠉⠉⠙⠛⠛
⢸⣿⡀⠄⠈⣹⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠿⠿⢿⣿⣿⣷⣾⣿
⢸⣿⣿⣿⣿⣿⣿⣿⣿⠛⢩⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⢸⣿⣿⣿⣿⣿⡿⣿⣿⣴⣿⣿⣿⣿⣄⣠⣴⣿⣷⣭⣻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⠸⠿⣿⣿⣿⠋⣴⡟⠋⠈⠻⠿⠿⠛⠛⠛⠛⠛⠃⣸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⢸⣿⣿⣿⡁⠈⠉⠄⠄⠄⠄⠄⣤⡄⠄⠄⠄⠄⠄⠈⠄⠈⠻⠿⠛⢿⣿⣿⣿⣿⣿
⢸⣿⣿⣿⠄⠄⠄⠄⠄⠄⠄⠄⣠⣄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢀⣀⣿⣿⣿⣿
⢸⣿⣿⣿⡀⠄⠄⠄⠄⠄⠄⠄⠉⠉⠁⠄⠄⠄⠄⠐⠒⠒⠄⠄⠄⠄⠉⢸⣿⣿⣿
⢸⣿⣿⣿⢿⣦⣄⣠⣄⠛⠟⠃⣀⣀⡀⠄⠄⣀⣀⣀⣀⣀⣀⡀⢀⣰⣦⣼⣿⣿⡿
⢸⣿⣿⣿⣿⣿⣿⣻⣿⠄⢰⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⢛⣥⣾⣟⣿⣿⣿⣿⣿
⢸⣿⣿⣿⣿⣿⣿⣿⣿⡆⠈⠿⠿⠿⠿⠿⠿⠿⠿⠿⣧⣶⣿⣿⣿⣿⣿⣿⣿⣿⣿
⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿ `"""
    )


@bot.command(aliases=['SUS'])
async def sus(ctx):
    await ctx.send(f'''`⡯⡯⡾⠝⠘⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢊⠘⡮⣣⠪⠢⡑⡌
⠀⠀⠀⠟⠝⠈⠀⠀⠀⠡⠀⠠⢈⠠⢐⢠⢂⢔⣐⢄⡂⢔⠀⡁⢉⠸⢨⢑⠕⡌
⠀⠀⡀⠁⠀⠀⠀⡀⢂⠡⠈⡔⣕⢮⣳⢯⣿⣻⣟⣯⣯⢷⣫⣆⡂⠀⠀⢐⠑⡌
⢀⠠⠐⠈⠀⢀⢂⠢⡂⠕⡁⣝⢮⣳⢽⡽⣾⣻⣿⣯⡯⣟⣞⢾⢜⢆⠀⡀⠀⠪
⣬⠂⠀⠀⢀⢂⢪⠨⢂⠥⣺⡪⣗⢗⣽⢽⡯⣿⣽⣷⢿⡽⡾⡽⣝⢎⠀⠀⠀⢡
⣿⠀⠀⠀⢂⠢⢂⢥⢱⡹⣪⢞⡵⣻⡪⡯⡯⣟⡾⣿⣻⡽⣯⡻⣪⠧⠑⠀⠁⢐
⣿⠀⠀⠀⠢⢑⠠⠑⠕⡝⡎⡗⡝⡎⣞⢽⡹⣕⢯⢻⠹⡹⢚⠝⡷⡽⡨⠀⠀⢔
⣿⡯⠀⢈⠈⢄⠂⠂⠐⠀⠌⠠⢑⠱⡱⡱⡑⢔⠁⠀⡀⠐⠐⠐⡡⡹⣪⠀⠀⢘
⣿⣽⠀⡀⡊⠀⠐⠨⠈⡁⠂⢈⠠⡱⡽⣷⡑⠁⠠⠑⠀⢉⢇⣤⢘⣪⢽⠀⢌⢎
⣿⢾⠀⢌⠌⠀⡁⠢⠂⠐⡀⠀⢀⢳⢽⣽⡺⣨⢄⣑⢉⢃⢭⡲⣕⡭⣹⠠⢐⢗
⣿⡗⠀⠢⠡⡱⡸⣔⢵⢱⢸⠈⠀⡪⣳⣳⢹⢜⡵⣱⢱⡱⣳⡹⣵⣻⢔⢅⢬⡷
⣷⡇⡂⠡⡑⢕⢕⠕⡑⠡⢂⢊⢐⢕⡝⡮⡧⡳⣝⢴⡐⣁⠃⡫⡒⣕⢏⡮⣷⡟
⣷⣻⣅⠑⢌⠢⠁⢐⠠⠑⡐⠐⠌⡪⠮⡫⠪⡪⡪⣺⢸⠰⠡⠠⠐⢱⠨⡪⡪⡰
⣯⢷⣟⣇⡂⡂⡌⡀⠀⠁⡂⠅⠂⠀⡑⡄⢇⠇⢝⡨⡠⡁⢐⠠⢀⢪⡐⡜⡪⡊
⣿⢽⡾⢹⡄⠕⡅⢇⠂⠑⣴⡬⣬⣬⣆⢮⣦⣷⣵⣷⡗⢃⢮⠱⡸⢰⢱⢸⢨⢌
⣯⢯⣟⠸⣳⡅⠜⠔⡌⡐⠈⠻⠟⣿⢿⣿⣿⠿⡻⣃⠢⣱⡳⡱⡩⢢⠣⡃⠢⠁
⡯⣟⣞⡇⡿⣽⡪⡘⡰⠨⢐⢀⠢⢢⢄⢤⣰⠼⡾⢕⢕⡵⣝⠎⢌⢪⠪⡘⡌⠀
⡯⣳⠯⠚⢊⠡⡂⢂⠨⠊⠔⡑⠬⡸⣘⢬⢪⣪⡺⡼⣕⢯⢞⢕⢝⠎⢻⢼⣀⠀
⠁⡂⠔⡁⡢⠣⢀⠢⠀⠅⠱⡐⡱⡘⡔⡕⡕⣲⡹⣎⡮⡏⡑⢜⢼⡱⢩⣗⣯⣟
⢀⢂⢑⠀⡂⡃⠅⠊⢄⢑⠠⠑⢕⢕⢝⢮⢺⢕⢟⢮⢊⢢⢱⢄⠃⣇⣞⢞⣞⢾
⢀⠢⡑⡀⢂⢊⠠⠁⡂⡐⠀⠅⡈⠪⠪⠪⠣⠫⠑⡁⢔⠕⣜⣜⢦⡰⡎⡯⡾⡽`''')

@bot.command(aliases=['emojis', 'emoji'])
async def overlays(ctx):
    valid = str(overlay_all)[1:-1]
    await ctx.send(f"""Valid overlays are: \n\t `{valid}`""")


@bot.command(aliases=["sponge", "spongebob"])
async def spongify(ctx, *, message):

    logger.info("SPONGEBOB TEXT")
    logger.info(message)
    bob = "".join(
        [[str.upper, str.lower][(i + 1) % 2](chr) for i, chr in enumerate(message)]
    )
    await ctx.send(f"`{bob}`")


@bot.command(aliases=["git", "branch"])
async def get_active_branch_name(ctx):

    head_dir = Path(".") / ".git" / "HEAD"
    with head_dir.open("r") as f:
        content = f.read().splitlines()

    for line in content:
        if line[0:4] == "ref:":
            branch = line.partition("refs/heads/")[2]
            await ctx.send(f"""current branch: `{branch}`""")


import os


@bot.command(aliases=overlay_all)
async def overlay_emoji(ctx, *, underlay=""):

    logger.info("command")

    prefix = ctx.message.content[2:].split(" ")[0]
    logger.info("prefix")
    logger.info(prefix)
    overlay = overlay_dict.get(prefix, prefix)

    logger.info(ctx.message.attachments)
    logger.info(overlay)

    print("content")
    logger.info(ctx.message.content)

    # if 'http' in ctx.message.content:

    #     url = ctx.message.content.split()[1]
    #     from urllib.request import urlopen
    #     image_formats = ("image/png", "image/jpeg", "image/gif", "video/mp4")

    #     site = urlopen(url)
    #     meta = site.info()  # get header of the http request
    #     if meta["content-type"] in image_formats:
    #         logger.info('can process!')
    #     logger.info(meta["content-type"])
    #     return

    if ctx.message.attachments and overlay in ["deepfry", "giffry"]:
        if os.path.isdir("fry/saved"):
            pass
        else:
            os.mkdir("fry/saved")
        logger.info("FRY IMAGE")
        img = ctx.message.attachments[0]
        logger.info("img")
        fp = "fry/saved/" + img.filename.replace("png", "jpg").replace("PNG", "jpg")

        await ctx.message.attachments[0].save(fp)
        logger.info(fp)
        if overlay == "giffry":
            fp = deep_fry(fp, gif=True)
        else:
            fp = deep_fry(fp)
        logger.info(fp)
        with open(fp, "rb") as fh:
            f = discord.File(fh, filename=fp)
            await ctx.send(file=f)
        await ctx.message.delete()
        return

    if not underlay:
        fp, emoji = roll_emoji(ctx)
        if emoji:
            await emoji.url.save(fp)

    elif len(underlay) < 3:
        logger.info(underlay)

        symbol = (
            underlay.encode("unicode-escape")
            .decode("ASCII")[2:]
            .lstrip("0")
            .split("\\")[0]
        )

        logger.info(symbol)
        logger.info(symbol)
        logger.info(symbol)
        fp = get_twitter_svg(symbol)
    else:
        logger.info(underlay)
        underlay = underlay[1:-1]
        logger.info(underlay.split(":"))
        _, emoji_name, emoji_id = underlay.split(":")
        emoji = bot.get_emoji(int(emoji_id))

        fp = f"hat/saved/{emoji_name}.png"
        await emoji.url.save(fp)

    logger.info("OVERLAY TIME")
    logger.info(overlay)
    logger.info(fp)

    if overlay == "deepfry":
        logger.info("overlay is deepfry")
        hatpath = deep_fry(fp)
        logger.info(hatpath)
    elif overlay == "giffry":
        logger.info("overlay is deepfry")
        hatpath = deep_fry(fp, gif=True)
        logger.info(hatpath)

    else:
        hatpath = overlay_hat(fp, overlay)
    with open(hatpath, "rb") as fh:
        f = discord.File(fh, filename=hatpath)
    await ctx.send(file=f)
    await ctx.message.delete()


def roll_emoji(ctx):
    import os
    import random

    random.seed()
    roll = random.random()
    emoji = None
    if roll > 0.75:
        custom = [e.id for e in ctx.guild.emojis]
        emoji_id = random.choice(custom)
        emoji = bot.get_emoji(int(emoji_id))
        logger.info(emoji.url)
        fp = f"hat/saved/{emoji_id}.png"

    elif roll > 0.25:
        standard = ["hat/svg/" + e for e in os.listdir("hat/svg/")]
        fp = random.choice(standard)
        logger.info(fp)
    else:
        import pickle

        standard = pickle.load(open("hat/emoji_list.pkl", "rb"))
        try:
            symbol = random.choice(standard)
            fp = get_twitter_svg(symbol)
            logger.info(fp)
        except Exception as e:
            logger.info(e)
            logger.info("get twitter failed")
            fp = get_local_svg(random.choice(standard))
    return fp, emoji


def get_twitter_svg(symbol):

    import requests

    webpath = f"https://twemoji.maxcdn.com/v/12.1.4/svg/{symbol}.svg"

    logger.info(webpath)
    r = requests.get(webpath)
    fp = f"hat/saved/{symbol}.svg"
    with open(fp, "wb") as f:
        f.write(r.content)
    return fp


def get_local_svg(symbol):

    # requires the /assets/svg/ folder from the  12.1.4 image pack
    # https://github.com/twitter/twemoji/releases/tag/v12.1.4
    # move folder to /hat/svg
    import os

    fp = f"hat/svg/{symbol}.svg"

    logger.info(os.path.exists("hat/svg/{symbol}.svg"))

    logger.info(fp)
    return fp


@bot.command()
async def numvotes(ctx):
    embed = discord.Embed(title="Number of guillotine votes")
    embed.add_field(
        inline=False,
        name="Number of required users to succesfully guillotine someone",  # , based on last 30 minutes of message sending activity",
        value=config[ctx.guild.id]["VOTING_RULES"]["muted"]["min_voters"],
    ),
    #     )
    await ctx.send(embed=embed)
    await ctx.message.delete()


# @bot.command()
# async def coomands(ctx):
#     await ctx.send('```' + ", ".join(sorted(list(img_commands.get_keys(ctx.guild.id)))) + '```')


@bot.command(aliases=["manual", "commands", "info"])
async def help(ctx):
    embed = discord.Embed(title="How to use this bot")

    # TODO I added the guild number for the guillotine emoji manually by finding it in the bot logs.
    # How to do this automatically and return the correct emoji?
    # Further it dispayed in the hover text as ":guillotine-1:" because the bot
    #   was using the guillotine in a second server. This will NOT scale.
    # TODO make this so it shows relative guillotine's count
    embed.add_field(
        inline=False,
        name="Vote to mute user",
        value="""
            If `{min_voters}` unique users react with the :{vote_emoji}: emoji within `{timeout}` seconds,
            the user will be muted from posting for `{duration}` seconds.
    

            """.format(
            **buffer_container.get_mute_config(ctx.guild.id)
        ),
    )

    embed.add_field(
        inline=False,
        name="Vote to gulag user",
        value="""
            If `{min_voters}` unique users react with the :{vote_emoji}: emoji within `{timeout}` seconds,
            the user will be exiled to the `#gulag` channel for `{duration}` seconds.
    

            """.format(
            **buffer_container.get_gulag_config(ctx.guild.id)
        ),
    )

    embed.add_field(
        inline=False,
        name="Add image to bot",
        value='`>>addimage "<command name>" "<img url>" "<img description>"`',
    )

    embed.add_field(
        inline=False, name="Delete an image from the bot", value="`>>delete <img name>`"
    )

    embed.add_field(
        inline=False, name="Search for an image", value="`>>enlightenme <search term>`"
    )
    embed.add_field(
        inline=False,
        name="Show image commands",
        value="`>>coomands` to see list. Run these like `>>beetax`",
    )
    await ctx.send(embed=embed)


@bot.command(aliases=["latency"])
async def ping(ctx):
    await ctx.send("`Bot latency: {}s`".format(round(bot.latency, 2)))


@bot.command()
async def members(ctx):
    membs = await ctx.guild.chunk()
    logger.info(membs)
    membs = f""" `{' '.join([m.name for m in membs])}`"""
    await ctx.send(membs)


hmoji = [
    "smirksweat",
    "ahegao",
    "hotdog",
    "<:smirksweat:765405723014070273>",
    "<:ahegao:735647692566429769>",
]

textmoji = ["🍑", "🍆", ">>ahe", "deez", "vaush"]
haram = hmoji + textmoji


@bot.listen("on_message")
async def autobonk(message):
    # 🐝-tax
    if message.author.bot:
        return
    if message.channel.name == "🐝-tax" or message.channel.name == "general":
        cleartext = str(message.content).lower().split()
        if any([h in cleartext for h in haram]) or any(
            [c.startswith("cum") for c in cleartext]
        ):
            bonk_emoji = random.choice(
                [
                    "sadcowboy",
                    "pingu",
                    "jokerkek",
                    "bonk",
                    "papajoe" "gooseknife",
                    "bidenmalarkey",
                ]
            )

            bonk_emoji_id = discord_get(message.guild.emojis, name=bonk_emoji)
            logger.info("BONK CHANNEL MSG")
            logger.info(message.channel.name)
            logger.info("HRONNESS DETECTED!!!")
            await message.add_reaction(bonk_emoji_id)
            logger.info(message.content)


@bot.event
async def on_raw_reaction_add(payload):
    logger.info("PAYLOAD")

    channel = bot.get_guild(payload.guild_id).get_channel(payload.channel_id)
    msg = await channel.fetch_message(payload.message_id)

    if payload.emoji.name == buffer_container.get_mute_emoji(payload.guild_id):

        await buffer_container.get_mute_buffer(payload.guild_id).check_validity(
            payload, bot.get_channel(payload.channel_id)
        )
        # leaderboard.inc_mute_reaction(payload.guild_id, msg.author.id)

    if payload.emoji.name == buffer_container.get_gulag_emoji(payload.guild_id):
        await buffer_container.get_gulag_buffer(payload.guild_id).check_validity(
            payload, bot.get_channel(payload.channel_id)
        )
        # leaderboard.inc_gulag_reaction(payload.guild_id, msg.author.id)

    elif payload.emoji.name == buffer_container.get_delete_emoji(payload.guild_id):
        await del_post(payload)
        # leaderboard.inc_del_reaction(payload.guild_id, msg.author.id)


async def del_post(payload):
    channel = bot.get_channel(payload.channel_id)
    message = await channel.fetch_message(payload.message_id)
    del_buffer = buffer_container.get_del_buffer(payload.guild_id)
    if (
        not [message.id, payload.member.id]
        in del_buffer.df[["message_id", "user"]].drop_duplicates().values.tolist()
    ):
        await del_buffer.append(
            {
                "user": payload.member.id,
                "message": message,
                "message_id": message.id,
                "time": time.time(),
                "channel": channel,
            }
        )
        # leaderboard.inc_del_reaction(payload.guild_id, payload.member.id)
    else:
        logger.info(f"USER {payload.member.name} ALREADY VOTED TO DELETE")


async def improper_usage(ctx):
    await ctx.send("Improper command usage! See `>>help` for more.")


# @bot.command()
# async def shibe(ctx):
#     with urlopen(Request(url="http://shibe.online/api/shibes?count=1&urls=true&httpsUrls=true", headers={'User-Agent': 'Mozilla/5.0'})) as json_return:
#         shibe_contents = json_return.read()
#         msg = "{0}, here is your random shibe:".format(ctx.message.author.name)
#         url = json.loads(shibe_contents)[0]
#     await ctx.send(embed=img_commands.imgfun(msg, url))


# @bot.command(aliases=['bird'])
# async def birb(ctx):
#     with urlopen(Request(url="http://random.birb.pw/tweet.json", headers={'User-Agent': 'Mozilla/5.0'})) as json_return:
#         # get image filename
#         birb_contents = json_return.read()
#         msg = "{0}, here is your random birb:".format(ctx.message.author.name)
#         # insert image filename into URL
#         url = "http://random.birb.pw/img/{}".format(
#             json.loads(birb_contents)["file"])
#     await ctx.send(embed=img_commands.imgfun(msg, url))


# @bot.command()
# async def refresh(ctx):
#     try:
#         if ctx.message.author.guild_permissions.administrator is True:
#             guild_config = db.get_config(ctx.guild.id)

#             buffer_container.refresh_config(ctx.guild.id, guild_config)
#             img_commands.refresh(ctx.guild.id)
#             init_global_config()
#             relative_guillotine.init(ctx.guild.id, guild_config["MIN_VOTES"])
#             if task_wrapper['task'] is not None:
#                 task_wrapper['task'].cancel()
#             task_wrapper['task'] = bot.loop.create_task(status_loop())
#             await(await ctx.send(f'{ctx.message.author.mention}: config for server successfully updated')).delete(delay=10)
#         else:
#             await(await ctx.send(f'{ctx.message.author.mention}: you must be an administrator to update the config')).delete(delay=10)
#     except Exception as e:
#         logger.info(e)
#         pass
#     finally:
#         await ctx.message.delete()


# @bot.command(aliases=['addimage'])
# async def add_image(ctx, *, message):
#     await img_commands.add_image(ctx, message)


# @bot.command()
# async def delete(ctx, arg1):
#     await img_commands.delete_img(ctx, arg1)


# @bot.command(aliases=['random'])
# async def rand(ctx):
#     await img_commands.get_random_img(ctx)


# @bot.command(aliases=['leaderboard'])
# async def leaders(ctx):
#     await leaderboard.print_leaderboard(ctx)


@tasks.loop(seconds=60)
async def disappear(ctx, disappear_channels):
    ids_ttl = {}
    logger.info(disappear_channels)
    for channel in ctx.guild.channels:
        if channel.name in disappear_channels:
            ids_ttl.update({channel: disappear_channels[channel.name]})

    for ch, ttl in ids_ttl.items():

        logger.info(f"SEARCHING MESSAGES FROM {ch.name} OLDER THAN {ttl}")
        
    
        async def transform(message):
            age = (datetime.datetime.utcnow() - message.created_at).total_seconds()
            logger.info(f'CURRENT TIME {datetime.datetime.utcnow()}')
            logger.info(f'CREATED AT {message.created_at}')
            if age > ttl:
                logger.info(f"DELETING MESSAGE {age}")
                await message.delete()
                await asyncio.sleep(1)
                return True
            else:
                logger.info(f"KEEPING MESSAGE {age}")
                return False

        history = await ch.history(oldest_first=True).flatten()
        logger.info(f'HISTORY - {len(history)} MESSAGES')

        if ch.name == "b-tax":
            for msg in history[:-200]:

                await msg.delete()
                await asyncio.sleep(0.5)

        else:
            for msg in history:
                if not await transform(msg):
                    logger.info("OUT OF MESSAGES")    
                    break


poster_running = 0


class NameGenerator:
    def __init__(self):
        self.records = {}

        self.time_reset = 24 * 3600
        self.records["epoch"] = self.epoch

    @property
    def epoch(self):

        # time_reset = (8*3600)
        return divmod(round(time.time()), self.time_reset)[0]

    async def get(self, author, guild):
        """names are determinsitic by user + day, but
        stored in hash table for cpu sake"""

        user_num = author.id + self.epoch

        logger.info(self.records)

        logger.info("EPOCH")
        logger.info(self.records["epoch"])

        ch = discord_get(guild.channels, name="b-tax")

        if len(self.records) == 1:
            await ch.send(f"`Names will reset every {self.time_reset//3600} hours.`")

        if user_num in self.records:
            colorname = self.records[user_num]
        else:
            # set seed for user + day

            colorname = self.generate_id(user_num)

        return colorname

    def generate_id(self, user_num):

        random.seed(user_num)

        a = "shiny radiant post-apocalyptic punished bisexual cosmic unreasonable big-structural smol windy mystical moldy sensual lucky cursed post-colonial sexy excessive fuzzy happy etherial astral thunderous revolutionary legendary spectral flying post drunken yoked pogging wet-ass based poggers pixelated grillpilled fierce smooth-brained galaxy-brained vicious sweet cozy chaotic pristine epic psychedelic vapid joyful recursive clever edgy rabid lilac salty optimized complex elegant abundant graceful recreational spicy post-structural smirking laughing promiscuous advanced unexpected frisky loving quantum reflective lunar substandard jealous prismatic charismatic chtonic virulent 100% torqued experimental "
        b = "resonant stochastic earthen interim-president simple victorious gulaged cottagecore party kpop radical inveterage naked hungry noided horny disenfranchised uncucked doomer ungovernable apolitical deep-fried cumming thicc musical catalonian smoked underground dancing friendly vibing alaskan blessed astroturfed k-holed northwest asian european contagious singing headless hegelian hexed larping postmodern enlightened rhizomatic tantric manic dream jarjar heckin anarcho leninist shitposting illiterate antifascist anprim soviet vaping vaporwave psychic mellow satanic sensual hexagonal magnum tasty absurd luminiferous synthetic subaltern shimmering bootleg nerfed perverted class-conscious liminal streaming superliminal lumpen gluten-free bourgeois pansexual herbal stellar stylish space neoliberal primal interdimensional immanent "
        c = "coomer guaido gator hippo q-anon-mom engles castro kanye hasan doge surfer pangolin joker nirvana ego-death subtweet god fanfic monopoly sanders pogchamp river sauce cause dino canyon voter nerd meme brunch election comrade guillotine pingu healer cowboy beetax ketamine ideology kitty fentanyl pumpkin class-traitor shibe furry musk alpha neomarxist anarchist cop tradwife tankie birb zoomer sicko posadist boomer bogan kevin lib praxisss jones supplement cheeto binks citizen pupper solidarity adrenochrome leopard dong goth gf dolphin WAP fetish nationalist streamer beta pixie gamer hacker chad monke labor fascist rainbow jaguar snowflake witch communist skeleton insanity proletarian catgirl human cyborg pylon seaweed cyberpunk cybergoth influencer tiger pogalien vaper geode pikachu piñero radlib cactus ivysaur supernova machine chasm void cosmology aether replicant id superego ego superstructure farmer singularity arpeggio martyr pogfish pepe chungus chakra vortex mood pp npc ahegao karen globalist dmt spectre merman load prestige hog reptile volcel worker mandy"

        colorname = " ".join([random.choice(_.split()) for _ in (a + b, c)])
        colorname = random_emoji()[0] + " " + f"`{colorname}` "

        ##
        # emoji = random.choice(bot.emojis)
        # colorname = f'<:{emoji.name}:{emoji.id}> ' + f'`{colorname}` '
        ##

        self.records[user_num] = colorname
        return colorname


name_gen = NameGenerator()


async def bot_tax(message):

    channel = discord_get(message.guild.channels, name="b-tax")
    logger.info("MESSAGE")
    logger.info(message.author.id)
    logger.info(message.content)

    if message.content.startswith(">>") or message.content.startswith("!"):
        return

    user = await name_gen.get(message.author, message.guild)
    await channel.send(user + message.content)

    if message.channel.type.name != "private" and message.channel.name == "whispers":
        await message.delete()


@bot.listen("on_message")
async def start_desa(message):
    if message.channel.type.name == "private":
        return
    disappear_channels = buffer_container.get_disappear_channels(message.guild.id)
    if message.channel.name in disappear_channels:
        logger.info("MESSAGE POSTED TO DISAPPEAR CHANNEL")
        logger.info(disappear.is_running())
        if not disappear.is_running():
            await disappear.start(message.channel, disappear_channels)


# @bot.command(aliases=['pic', 'pics'])
# async def enlightenme(ctx, message):
#     logger.info('ENLIGHTENME')
#     logger.info(message)
#     img_data = img_api.get_random_img(message, 50)
#     await ctx.channel.send(embed=img_commands.imgfun(img_data["title"], img_data["image"]))

from discord.utils import get as discord_get


@bot.command("bootstrap")
async def bootstrap(ctx):
    guild = ctx.message.guild
    channels = "horny-jail-🍒", "desaparecido", "whispers", "b-tax"
    guild_ch_names = [c.name for c in guild.text_channels]
    logger.info(guild_ch_names)

    for ch in channels:

        if ch not in guild_ch_names:
            await guild.create_text_channel(ch)

    btax = discord_get(guild.text_channels, name="b-tax")
    whispers = discord_get(guild.text_channels, name="whispers")
    everyone = discord_get(guild.roles, name="@everyone")

    await whispers.set_permissions(everyone, read_messages=False, send_messages=True)

    await btax.set_permissions(everyone, read_messages=True, send_messages=False)

    emojis = {
        "sadcowboy": "https://cdn.discordapp.com/emojis/731335282204344361.png",
        "bonk": "https://cdn.discordapp.com/emojis/774035533612711937.png",
        "jokerkek": "https://cdn.discordapp.com/emojis/773414158275837982.png",
        "papajoe": "https://cdn.discordapp.com/emojis/732123787360993316.png",
        "gooseknife": "https://cdn.discordapp.com/emojis/780319372794396701.png",
        "bidenmalarkey": "https://cdn.discordapp.com/emojis/800247267378331678.png",
        "guillotine": "https://cdn.discordapp.com/emojis/731344815907930113.png",
        "doomjack": "https://cdn.discordapp.com/emojis/845596368935845928.png",
        "pingu": "https://cdn.discordapp.com/emojis/736133041629691935.png",
    }

    for k, v in emojis.items():
        if discord_get(guild.emojis, name=k) is None:
            import requests as r

            logger.info(f"adding emoji {k}")
            img = r.get(v, stream=True).content
            try:

                await guild.create_custom_emoji(name=k, image=img)
            except Exception as e:
                logger.warning(e)

                continue


@bot.listen("on_message")
async def coommand(message):

    if (
        message.channel.type.name == "private"
        and message.author.name != "flying guillotine"
    ):
        await bot_tax(message)

    if message.channel.name == "whispers":
        await bot_tax(message)

    if not message.content.startswith(">>"):
        return

    key = message.content[2:]
    if bot.get_command(key) is not None:
        return

    # if img_commands.has_key(message.guild.id, key):
    #     await img_commands.send_img(message.guild.id, key, message.channel.send)
    # try:
    #     # try to delete command message
    #     await message.delete()
    # except AttributeError:
    #     pass


@bot.command("clear")
async def clear_text(ctx, *, line_count):

    logger.info(line_count)
    if int(line_count) < 50:
        if discord_get(ctx.author.roles, name="admin") is not None:
            await ctx.channel.purge(limit=int(line_count))
        else:
            await ctx.send("`only admins can delete history` :triumph: ")
    elif int(line_count) >= 50:
        await ctx.send("`please try to delete in batches of 50 or less` :pray:")


@tasks.loop(hours=48)
async def guillotine_role_perms():
    muted_role = None
    for guild in bot.guilds:
        muted_role = discord_get(guild.roles, name="guillotined")

        if muted_role is None:
            await guild.create_role(name="guillotined")
            muted_role = discord_get(guild.roles, name="guillotined")

        from discord import Color

        await muted_role.edit(color=Color.red())

        for category in guild.categories:
            await category.set_permissions(
                muted_role, read_messages=True, send_messages=False, add_reactions=True
            )


@tasks.loop(minutes=30)
async def postertariat():

    for guild in bot.guilds:
        logger.info(guild)
        # TODO create posterteriat if it doesn exist
        # logger.info(guild.roles)

        poster = discord_get(guild.roles, name="posterteriat")

        everyone = discord_get(guild.roles, name="@everyone")

        if poster is None:
            await guild.create_role(name="posterteriat")
            poster = discord_get(guild.roles, name="posterteriat")

        # perms = discord.Permissions(1067537780289)

        # await poster.edit(permissions=perms)

        await everyone.edit(
            permissions=discord.Permissions(read_messages=False, send_messages=False)
        )
        logger.info("poster.permissions")
        logger.info(poster.permissions)

        gulag = discord_get(guild.roles, name="gulag")

        logger.info(poster)
        logger.info(gulag)

        for user in guild.members:
            if (
                user not in buffer_container.get_gulag_buffer(guild.id).users_with_role
                and gulag not in user.roles
            ):
                await asyncio.sleep(5)
                await user.add_roles(poster)
                if discord_get(user.roles, name="gulag") is not None:
                    await user.remove_roles(gulag)


@bot.event
async def on_member_join(member):

    poster = discord_get(member.guild.roles, name="posterteriat")
    logger.info(poster)
    await member.add_roles(poster)


if __name__ == "__main__":
    logger.info(f"discord version {discord.__version__}")
    bot.run(tokens["bot_token"])
