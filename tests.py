import discord
import discord.ext.test as dpytest
import pytest
import asyncio
import time
import json
import bot as fg
from pandas import DataFrame
from functools import partial
from lib import relative_guillotine, img_commands, buffer_container

import logging


pytestmark = pytest.mark.asyncio
with open('config.json', 'r') as json_file:
    config = json.load(json_file)

# idk how to do mocks in python, there is probably a better way to do this
# but this works so :shrug:
async def send_img(guild_id, command, send_fn, index=None, msg_override=None):
    print('yay')

def init_buffers(guild_id):
    relative_guillotine.init(guild_id, 3)
    buffer_container.init_buffer(guild_id, config)
#
# FIXTURES
#
@pytest.fixture
async def bot():
    import bot as fg
    bot = fg.bot

    dpytest.configure(bot)
    me = bot.guilds[0].me
    channel = bot.guilds[0].text_channels[0]
    img_commands.send_img = send_img
    admin_role = await channel.guild.create_role(name="admin")
    await dpytest.runner.set_permission_overrides(
        me, channel, manage_roles=True)
    return bot

@pytest.fixture
def channel_ctx(bot):
    guild_ctx = bot.guilds[0]
    return guild_ctx.text_channels[0]

@pytest.fixture
def mute_buffer(channel_ctx):
    init_buffers(channel_ctx.guild.id)
    mute_buffer = buffer_container.get_mute_buffer(channel_ctx.guild.id)
    mute_buffer.df = DataFrame()
    return mute_buffer

@pytest.fixture
def del_buffer(channel_ctx):
    init_buffers(channel_ctx.guild.id)
    del_buffer = buffer_container.get_del_buffer(channel_ctx.guild.id)
    del_buffer.df = DataFrame(columns=["user"])
    return del_buffer

@pytest.fixture
async def msg(bot, channel_ctx):
    non_admin = build_member(1000, bot)
    message = dpytest.backend.make_message(
        content="A distasteful message",
        author=non_admin,
        channel=channel_ctx)

    await dpytest.backend._dispatch_event("send_message", message)
    return message

#these are just used in test classes to have access
#to fixtures on the instance, so they don't
#need to be injected every method
def _inject(cls, names):
    @pytest.fixture(autouse=True)
    def auto_injector_fixture(self, request):
        for name in names:
            setattr(self, name, request.getfixturevalue(name))

    cls.__auto_injector_fixture = auto_injector_fixture
    return cls

def auto_inject_fixtures(*names):
    return partial(_inject, names=names)


#
# BUFFER TESTS
#
@auto_inject_fixtures('bot', 'channel_ctx', 'msg', 'mute_buffer')
class TestMuteBuffer:

    def setup_method(self):
        self.config = config["VOTING_RULES"]["muted"]

    async def add_mute_row(self, user_id, msg=None):
        reacting_user = build_member(user_id, self.bot)
        if not msg:
            msg = self.msg
        new_row = {
            "reactor": reacting_user,
            "reactor_str": str(reacting_user),
            "channel": self.channel_ctx,
            "message_author": msg.author,
            "message_author_str": str(msg.author),
            "time": time.time(),
            "channel": self.channel_ctx
        }
        await self.mute_buffer.append(new_row, self.channel_ctx.guild.id)

    async def test_append(self):
        assert len(self.mute_buffer.df.index) == 0
        await self.add_mute_row(1)
        assert len(self.mute_buffer.df.index) == 1

    async def test_prune_time(self):
        await self.add_mute_row(1)
        self.mute_buffer.df.loc[0, "time"]  -= (self.config["timeout"] + 1)
        self.mute_buffer.prune_time()

        assert len(self.mute_buffer.df.index) == 0

    async def test_analyze(self):
        self.mute_buffer.duration = 5
        #non-admin
        await self.reach_mute_quorum(self.msg)
        assert len(self.mute_buffer.users_with_role) == 1
        #sleep so the buffer gets cleaned
        await asyncio.sleep(15)
        assert len(self.mute_buffer.users_with_role) == 0
        await self.clear_tasks()
        self.mute_buffer.duration = config['VOTING_RULES']['muted']['duration']

    async def reach_mute_quorum(self, msg):
        for i in range(1, relative_guillotine.get_min_voters(self.channel_ctx.guild.id)):
            await self.add_mute_row(i, msg)
        assert not self.mute_buffer.users_with_role
        final_mute = asyncio.create_task(
            self.add_mute_row(relative_guillotine.get_min_voters(self.channel_ctx.guild.id))
        )

        #this is hacky and might fail -- just need to
        #wait for all queued tasks to execute
        await asyncio.sleep(3)

    async def clear_tasks(self):
        for task in asyncio.all_tasks():
            task.cancel()
            try:
                await task
            except asyncio.CancelledError:
                print("Task cancelled")


@auto_inject_fixtures('msg', 'bot', 'channel_ctx', 'del_buffer')
class TestDeleteBuffer:

    async def add_del_row(self, user_id):
        reacting_user = build_member(user_id, self.bot)
        new_row = {
            "user": str(reacting_user),
            "message": self.msg,
            "message_id": self.msg.id,
            "time": time.time()
        }
        await self.del_buffer.append(new_row)

    async def test_append(self):
        assert len(self.del_buffer.df.index) == 0
        await self.add_del_row(1)
        assert len(self.del_buffer.df.index) == 1

    async def test_prune_time(self):
        await self.add_del_row(1)
        self.del_buffer.df.loc[0, "time"]  -= (self.del_buffer.timeout + 1)
        self.del_buffer.prune_time()

        assert len(self.del_buffer.df.index) == 0

    async def test_analyze(self):
        for i in range(1, self.del_buffer.DEL_VOTERS):
            await self.add_del_row(i)

        assert self.channel_ctx.last_message == self.msg

        """
        since embed in dpytest is broken
        this will break the tests
        uncomment when it's available
        """
        #await self.add_del_row(fg.DEL_VOTERS)
        #assert the message is gone

@auto_inject_fixtures('bot', 'channel_ctx', 'msg', 'mute_buffer')
class TestVoterValidity:

    def setup_method(self):
        self.config = config["VOTING_RULES"]["muted"]


    def manual_setup(self):
        self.user = build_member(1, self.bot)
        self.reaction = build_reaction(
            self.msg, self.user, self.config["vote_emoji"])

    async def test_forbidden(self):
        self.manual_setup()
        non_vote_roles = []

        if self.config["cant_vote"]:
            for role in self.config["cant_vote"]:
                role_obj = await self.bot.guilds[0].create_role(name=role)
                non_vote_roles.append(role_obj)
                await self.user.add_role(role_obj)

            await self.mute_buffer.check_validity(self.reaction)
            assert len(self.mute_buffer.df.index) == 0


    async def test_voters_rights(self):
        self.manual_setup()
        await self.mute_buffer.check_validity(
            self.reaction, self.channel_ctx)
        assert len(self.mute_buffer.df.index) == 1

    async def test_no_voters_rights(self):
        nonvoter = dpytest.backend.make_user(
            username="user-{}".format(id),
            discrim=id
        )
        nonvoter_react = build_reaction(
            self.msg, nonvoter, self.config["vote_emoji"])

        await self.mute_buffer.check_validity(
            nonvoter_react, self.channel_ctx)
        assert len(self.mute_buffer.df.index) == 0

    async def test_self_authored(self):
        self_react = build_reaction(
            self.msg, self.msg.author, self.config["vote_emoji"])

        await self.mute_buffer.check_validity(
            self_react, self.channel_ctx)
        assert len(self.mute_buffer.df.index) == 0

#
# UNIT TESTS
#
def test_print(caplog):
    fg.print("bee tax")
    assert "bee tax" in caplog.text

async def test_ping(bot, channel_ctx):
    await dpytest.empty_queue()
    await fg.ping(channel_ctx)
    correct_message = "`Bot latency: {}s`".format(round(bot.latency, 2))
    dpytest.verify_message(correct_message)

async def test_on_raw_reaction_add(bot, channel_ctx, msg, del_buffer, mute_buffer):
    user = build_member(1, bot)
    mute_emoji = config["VOTING_RULES"]["muted"]["vote_emoji"]
    #assert it goes to mute when mute react is used
    mute_react = build_reaction(msg, user, mute_emoji)
    await fg.on_raw_reaction_add(mute_react)
    mute_count = len(buffer_container.get_mute_buffer(channel_ctx.guild.id).df.index)
    assert mute_count > 0

    del_emoji = config["DEL_EMOJI"]
    #assert it goes to del when del react is used
    del_react = build_reaction(msg, user, del_emoji)
    await fg.on_raw_reaction_add(del_react)
    del_count = len(buffer_container.get_del_buffer(channel_ctx.guild.id).df.index)
    assert del_count > 0

    #assert it doesn't do anything with the wrong emoji
    neutral_react = build_reaction(msg, user, "nonsense_emoji")
    await fg.on_raw_reaction_add(neutral_react)
    assert (
        len(mute_buffer.df.index) == mute_count and \
        len(buffer_container.get_del_buffer(channel_ctx.guild.id).df.index) == del_count)

async def test_suspend_and_return(bot, channel_ctx, mute_buffer):
    target_user = build_member(1, bot)
    muted_role = await channel_ctx.guild.create_role(name="guillotined")
    await muted_role.edit(
        position=channel_ctx.guild.get_member(target_user.id).top_role.position+1)
    await dpytest.backend._dispatch_event("add_role", target_user, muted_role)
    await channel_ctx.set_permissions(muted_role, read_messages=True, send_messages=False, add_reactions=True)
    mute_buffer.duration = 1
    mute_buffer.users_with_role = [target_user]
    await mute_buffer.suspend_and_return(
        channel_ctx, target_user, muted_role)
    assert muted_role not in target_user.roles
    assert not mute_buffer.users_with_role
    mute_buffer.duration = config['VOTING_RULES']['muted']['duration']

async def test_del_post():
    pass

async def test_improper_usage():
    pass

#
# NOT FEASIBLE TO TEST RIGHT NOW
#
async def test_help(bot, channel_ctx):
    """
    There's unfortunately a bug with dpytest and embeds.
    There's an open PR that resolves it but for now, will
    have to leave this and the embed-involving commands
    below empty
    """
    pass

async def test_shibe():
    pass

async def test_birb():
    pass

async def test_add_image():
    pass

async def test_delete():
    pass

async def test_status_loop():
    """
    Not sure where to start with testing loops, et al.
    Some asyncio manipulation of the task queue probably.
    Same for below.
    """
    pass

async def test_on_ready():
    pass


##
## MOCK CLASSES & HELPERS
##
class TestEmoji:
    def add_emoji(self, name):
        self.name = name

def build_reaction(msg, user, emoji):
    """
    using raw models directly is hacky but only way
    to approximate the payload used for callbacks
    """
    emoji_obj = TestEmoji()
    emoji_obj.add_emoji(emoji)
    react_event = discord.raw_models.RawReactionActionEvent(
        data = {"message_id": msg.id,
                "channel_id": msg.channel.id,
                "guild_id": msg.guild.id,
                "user_id": user.id},
        emoji = emoji_obj,
        event_type = "REACTION_ADD")
    react_event.member = user

    return react_event

def build_member(id, bot):
    user = dpytest.backend.make_user(
        username="user-{}".format(id),
        discrim=id
    )

    vote_roles = [role for role in bot.guilds[0].roles \
                  if role.name in \
                  config["VOTING_RULES"]["muted"]["can_vote"]]

    return dpytest.backend.make_member(
        user, bot.guilds[0], roles=vote_roles)

