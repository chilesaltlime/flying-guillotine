from os import path

import cairosvg
import numpy as np
from PIL import Image, ImageEnhance, ImageOps, ImageSequence


def deep_fry(emoji_path, gif=False):
    """ handler for deep fryer to talk to bot """

    print(f"frying up {emoji_path}")
    colors.create_colors()
    if emoji_path.endswith("svg"):
        emoji_path = svg_png(emoji_path)

    outname = "fry/saved/_" + path.basename(emoji_path)
    img = Image.open(emoji_path)

    # if alpha:
    # img.putalpha(alpha)

    if emoji_path.endswith("gif"):
        fry_gif(img, outname)

    elif gif:
        outname = outname.split(".")[0] + ".gif"
        gif_fry(img, outname)

    else:
        img = Image.open(emoji_path)
        fryer(img, outname)
    return outname


def get_alpha(img):
    alpha = None
    if img.mode in ("RGBA", "LA") or (img.mode == "P" and "transparency" in img.info):
        alpha = img.convert("RGBA").split()[-1]
    return alpha


def resize(img):
    max_width = 400
    s = np.asarray(img.size)
    if s[0] < max_width:
        return img
    ratio = max_width / s[0]

    new_size = int(s[0] * ratio), int(s[1] * ratio)

    img = img.resize(new_size)
    return img


def fryer(img, outname="fry/saved/temp.jpg", rainbow=True, use_alpha=True):
    """ various functions for frying an image """

    if rainbow:
        colors.iterate_colors()
    if use_alpha:
        alpha = get_alpha(img)
    else:
        alpha = None
    img = img.convert("RGB")
    img = resize(img)

    r = img.split()[0]
    r = ImageEnhance.Contrast(r).enhance(5.0)
    r = ImageEnhance.Brightness(r).enhance(2.5)

    r = ImageOps.colorize(r, colors.color_a, colors.color_b)

    def process(img, n=3):
        """ colorize, bitcrush, sharpen repeatedly """

        for i in range(n):

            img = Image.blend(img, r, 0.4)
            posterize = np.random.randint(60, 70) / 10

            img = ImageOps.posterize(img, 3)
            img = ImageEnhance.Sharpness(img).enhance(80)

        img.save(outname, "jpeg", quality=100)
        return img

    def reopen(img_re, q=20, n=5):
        """ saves image with lossy compression repeatedly """

        for i in range(n):
            img_re.save(outname, "jpeg", quality=q)
            img_re = Image.open(outname)

        return img_re

    def noise(img_noise, scalar=1.75):
        """ convert the image to numpy for some noise """
        img_np = np.copy(np.array(img_noise))

        def scanlines(img_np):

            """ add horizontal scanlines """
            lines = np.random.randint(0, 2)
            coords = np.array([np.random.randint(0, img_np.shape[0], int(lines))])
            img_np[coords] += 128
            return img_np

        img_np = scanlines(img_np)

        def gauss(img_np, scalar):
            """generate a noise mask with same dimensions as image,
            multiply image color values by noise mask according to a scalar"""
            w, h, d = img_np.shape
            a = np.random.rand(w, h, d)
            a = np.ceil(a * scalar).astype("uint8") % 255
            img_np *= a
            return img_np

        img_np = gauss(img_np, scalar)
        noise_img = Image.fromarray(img_np)
        noise_img.save(outname, "jpeg", quality=200)

        return noise_img

    def boost_contrast(img):
        enhancer = ImageEnhance.Contrast(img)
        contrast = np.random.randint(10, 40) / 10

        img = enhancer.enhance(contrast)
        return img

    # process the image iteratively

    img = process(img, 1)
    img = reopen(img, 95, 1)
    img = boost_contrast(img)
    img = noise(img)
    if alpha:
        print(alpha)
        img.putalpha(alpha)
        img.save(outname, "png")
    return img


def svg_png(emoji_path):
    """ helper to convert standard emojis to png """

    print("emoji_path")
    temp = emoji_path.replace(".svg", ".png")
    print(emoji_path)
    print(temp)
    cairosvg.svg2png(url=emoji_path, write_to=temp, output_width=128, output_height=128)
    emoji_path = temp
    return emoji_path


def gif_fry(img, outpath, n=16):
    """ make a gif """

    images = [fryer(img, rainbow=True, use_alpha=False) for i in range(n)]

    def color_sort(img):
        import colorsys

        img = img.convert("RGB")
        img.resize((1, 1), resample=0)
        dominant_color = img.getpixel((0, 0))

        hsv = colorsys.rgb_to_hsv(*dominant_color)
        return hsv

    images.sort(key=color_sort)
    duration = 1000 // len(images) + 100

    images[0].save(
        outpath, save_all=True, append_images=images[1:], duration=duration, loop=0
    )


def fry_gif(img, outpath):
    """ fry a gif """

    frames = []
    print(img.info)
    print(img.n_frames)

    for frame in ImageSequence.Iterator(img):

        fr = fryer(frame, "temp", rainbow=False)
        frames.append(fr.convert("P"))

    print(len(frames))
    frames[0].save(
        outpath,
        save_all=True,
        append_images=frames[1:],
        duration=img.info["duration"],
        optimize=True,
        loop=0,
    )


class Colors:
    def __init__(self):
        self.create_colors()

    def create_colors(self):
        offset = 0
        self.color_a = np.random.randint(0, high=255, size=3)
        self.color_b = (self.color_a - offset) % 255

        print(self.color_b)

    def iterate_colors(self):

        scale = 32
        delta = np.random.randint(-scale, high=scale, size=3)
        self.color_a += delta
        self.color_b += delta

    def __call__(self):
        self.create_colors()


colors = Colors()


if __name__ == "__main__":
    # from PIL import GifImagePlugin
    deep_fry("fry/saved/amogus.png", gif=False)
