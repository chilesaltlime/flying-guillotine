import discord
import time
import re
import random

from lib import db

reaccs = {}
used_reactions = {}

def init(guild_id):
    reaccs[guild_id] = db.get_image_data(guild_id)
    used_reactions[guild_id] = {}

def refresh(guild_id):
    reaccs[guild_id] = db.get_image_data(guild_id)

async def send_img(guild_id, command, send_fn, index=None, msg_override=''):
    if command not in used_reactions[guild_id]:
        used_reactions[guild_id][command] = {}
    if index == None:
        if len(used_reactions[guild_id][command]) is len(reaccs[guild_id][command]):
            used_reactions[guild_id][command] = {} # reset bc we used all the reactions
        cont = True
        index = 0
        while cont:
            index = random.randint(0, len(reaccs[guild_id][command]) - 1)
            if index not in used_reactions[guild_id][command]:
                cont = False
                used_reactions[guild_id][command][index] = True
    message = reaccs[guild_id][command][index]["message"]
    await send_fn(msg_override, embed=imgfun(message, reaccs[guild_id][command][index]["url"]))
    db.increment_img_command(guild_id, command, index)


def imgfun(msg: str, img_url: str):
    return discord.Embed(
        title=msg, color=random.randint(0, 16777215)
    ).set_image(url=img_url)

async def add_image(ctx, message):
    global reaccs
    lst = re.findall(r'"(.*?)(?<!\\)"', message)
    last = 0
    if lst[0] in reaccs[ctx.guild.id]:
        last = len(reaccs[ctx.guild.id][lst[0]])
    db.add_img_ref(ctx.guild.id, lst[0], lst[1], lst[2])
    time.sleep(1)
    reaccs[ctx.guild.id] = db.get_image_data(ctx.guild.id)
    await send_img(ctx.guild.id, lst[0], ctx.send, last)

async def delete_img(ctx, arg1):
    db.delete_img_ref(ctx.guild.id, arg1)
    time.sleep(1)
    global reaccs
    reaccs[ctx.guild_id] = db.get_image_data(ctx.guild.id)

async def get_random_img(ctx):
    keys = list(reaccs[ctx.guild.id].keys())
    index = random.randint(0, len(keys) - 1)
    key = keys[index]
    sub_keys = reaccs[ctx.guild.id][key]
    sub_index = random.randint(0, len(sub_keys) - 1)
    await send_img(ctx.guild.id, key, ctx.send,  sub_index)

def has_key(guild_id, key):
    return reaccs[guild_id][key] is not None

def get_keys(guild_id):
    return reaccs[guild_id].keys()