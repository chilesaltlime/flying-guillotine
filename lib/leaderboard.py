import discord


from lib import db

# top 5
# at mention
# full 



leaderboard_types = ["mutes", "gulags", "deletes"]

leaderboard_desc = {
    "mutes": "guillotines",
    "gulags": "gulags",
    "deletes": "messages deleted"
}

async def print_leaderboard(ctx, user_id=None):
    if user_id is None:
        embed = discord.Embed(title="Look at all this shit posting")
        for lb_type in leaderboard_types:
            leaderboard = db.get_leaderboard(ctx.guild.id, lb_type)
            if leaderboard is not None:             
                sorted_leaders = sorted(leaderboard.items(), key=lambda x: x[1]["executions"] if "executions" in x[1] else 0, reverse=True)[:5]
                leader_str = ""
                for leader in sorted_leaders:
                    key = leader[0]
                    value = leader[1]
                    user = ctx.message.guild.get_member(int(key))
                    leader_str += "{user_name}: executed {executions} times, voted {votes} times\n".format(user_name = user.nick if user.nick is not None else user.name, executions = value["executions"] if "executions" in value else 0, votes = value["reactions"])
                embed.add_field(inline=False,
                    name=leaderboard_desc[lb_type],
                    value=leader_str)
        await ctx.send(embed=embed)

def inc_mute_reaction(guild_id, user_id):
    db.increment_leaderboard_react(guild_id, "mutes", user_id)

def inc_gulag_reaction(guild_id, user_id):
    db.increment_leaderboard_react(guild_id, "gulags", user_id)

def inc_del_reaction(guild_id, user_id):
    db.increment_leaderboard_react(guild_id, "deletes", user_id)

def inc_mute_execution(guild_id, user_id):
    db.increment_leaderboard_execution(guild_id, "mutes", user_id)

def inc_gulag_execution(guild_id, user_id):
    db.increment_leaderboard_execution(guild_id, "gulags", user_id)

def inc_del_execution(guild_id, user_id):
    db.increment_leaderboard_execution(guild_id, "deletes", user_id)
