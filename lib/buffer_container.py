from buffers import MuteBuffer, GulagBuffer, DelBuffer

buffers = {}
configs = {}

def init_buffer(guild_id, guild_config):
    global configs
    
    configs[guild_id] = guild_config
    print(configs)
    buffers[guild_id] = {}
    buffers[guild_id]["mute"] = MuteBuffer(get_mute_config(guild_id), "mute")
    buffers[guild_id]["gulag"] = GulagBuffer(get_gulag_config(guild_id), "gulag")
    buffers[guild_id]["delete"] = DelBuffer(timeout=guild_config["DEL_TIME"], votes=guild_config["DEL_VOTERS"])

def refresh_config(guild_id, config):
    global configs
    
    configs[guild_id] = config
    buffers[guild_id]["mute"].update_config(get_mute_config(guild_id))
    buffers[guild_id]["gulag"].update_config(get_gulag_config(guild_id))
    # TODO: fix this bc it reads config differently
    buffers[guild_id]["delete"].update_config(timeout=config["DEL_TIME"], votes=config["DEL_VOTERS"])

def get_disappear_channels(guild_id):
    
    return configs[guild_id]["DISAPPEAR_CHANNELS"]

def get_mute_buffer(guild_id):
    
    return buffers[guild_id]["mute"]

def get_gulag_buffer(guild_id):
    
    return buffers[guild_id]["gulag"]

def get_del_buffer(guild_id):
    
    return buffers[guild_id]["delete"]

def get_voting_rules(guild_id):
    
    return configs[guild_id]["VOTING_RULES"]

def get_mute_config(guild_id):
    
    return get_voting_rules(guild_id)["muted"]

def get_mute_emoji(guild_id):
    
    return get_mute_config(guild_id)["vote_emoji"]

def get_gulag_config(guild_id):
    
    return get_voting_rules(guild_id)["gulag"]

def get_gulag_emoji(guild_id):
    
    return get_gulag_config(guild_id)["vote_emoji"]

def get_delete_emoji(guild_id):
    
    return configs[guild_id]["DEL_EMOJI"]