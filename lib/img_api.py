import requests
import re
import json
import time
import random

def search(keywords, max_results=None):
    url = 'https://duckduckgo.com/';
    params = {
    	'q': keywords
    };

    #   First make a request to above URL, and parse out the 'vqd'
    #   This is a special token, which should be used in the subsequent request
    res = requests.post(url, data=params)
    searchObj = re.search(r'vqd=([\d-]+)\&', res.text, re.M|re.I);

    if not searchObj:
        return -1;


    headers = {
        'authority': 'duckduckgo.com',
        'accept': 'application/json, text/javascript, */*; q=0.01',
        'sec-fetch-dest': 'empty',
        'x-requested-with': 'XMLHttpRequest',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'cors',
        'referer': 'https://duckduckgo.com/',
        'accept-language': 'en-US,en;q=0.9',
    }

    params = (
        ('l', 'us-en'),
        ('o', 'json'),
        ('q', keywords),
        ('vqd', searchObj.group(1)),
        ('f', ',,,'),
        ('kp', '1'),
        ('v7exp', 'a'),
    )

    requestUrl = url + "i.js"
    response = []
    go = True

    while go:
        res = requests.get(requestUrl, headers=headers, params=params);
        data = json.loads(res.text);
        if "results" in data:
            response.extend(data["results"])

        if "next" not in data:
            go = False
            break

        requestUrl = url + data["next"];
    return response

def get_random_img(keywords, max_results):
    res = search(keywords, max_results)
    return res[random.randint(0, len(res))]