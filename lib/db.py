import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import json
import pathlib
from os import getenv

if getenv('TEST_FIREBASE_DB'):
    firebase_admin.initialize_app(options={
        'databaseURL': getenv('TEST_FIREBASE_DB')
    })
else:
    cred = credentials.Certificate(str(pathlib.Path().absolute()) + '/firebase_admin.json')
    firebase_admin.initialize_app(cred, {
        'databaseURL': 'https://reaccs-c6ff7.firebaseio.com/'
    })

def get_image_data(guild_id):
    ref = db.reference(str(guild_id)).child('reactions')
    return ref.get()

def add_img_ref(guild_id, command, url, message):
    ref = db.reference(str(guild_id)).child('reactions')
    existing = ref.get()
    urls = []
    if command in existing:
        oldUrls = existing[command]
        oldUrls.append({'url': url, 'message': message, 'count': 0})
        urls = oldUrls
    else:
        urls = [{'url': url, 'message': message, 'count': 0}]
    ref.child(command).set(urls)

def get_global_config():
    ref = db.reference('config')
    return ref.get()

def get_config(guild_id):
    ref = db.reference(str(guild_id)).child('config')
    return ref.get()

# def add_to_leaderboard(guild_id, board_key, user_id):
#     ref = db.reference(str(guild_id)).child('leaderboards').child(board_key)
#     data = ref.get()
#     if user_id in data:
#         data[user_id] = data[user_id] + 1
#     else:
#         data[user_id] = 1
#     ref.child(user_id).set(data[user_id])

def get_guild_prefix(guild_id):
    return f"{str(guild_id)}"

def get_img_command_prefix(guild_id, command):
    return f"{get_guild_prefix(guild_id)}/reactions/{command}"

def delete_img_ref(guild_id, command):
    ref = db.reference(get_img_command_prefix(guild_id, command))
    ref.delete()

def increment_img_command(guild_id, command, index):
    ref = db.reference(f"{get_img_command_prefix(guild_id, command)}/{str(index)}/count")
    count = ref.get() + 1
    ref.set(count)

# def get_leaderboard_prefix(guild_id, leaderboard_type):
#     return f"{get_guild_prefix(guild_id)}/leaderboard/{leaderboard_type}"

# def get_user_from_leaderboard_prefix(guild_id, leaderboard_type, user_id):
#     return f"{get_leaderboard_prefix(guild_id, leaderboard_type)}/{str(user_id)}"

# def get_leaderboard(guild_id, leaderboard_type):
#     ref = db.reference(get_leaderboard_prefix(guild_id, leaderboard_type))
#     return ref.get()

# def increment_leaderboard_react(guild_id, leaderboard_type, user_id):
#     ref = db.reference(get_user_from_leaderboard_prefix(guild_id, leaderboard_type, user_id))
#     resolved_ref = ref.get()
#     num = resolved_ref["reactions"] + 1 if resolved_ref is not None and "reactions" in resolved_ref else 1
#     ref.child("reactions").set(num)

# def increment_leaderboard_execution(guild_id, leaderboard_type, user_id):
#     ref = db.reference(get_user_from_leaderboard_prefix(guild_id, leaderboard_type, user_id))
#     resolved_ref = ref.get()
#     num = resolved_ref["executions"] + 1 if resolved_ref is not None and "executions" in resolved_ref else 1
#     ref.child("executions").set(num)
