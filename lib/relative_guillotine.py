import time
from datetime import datetime
import math

user_activity_map = {}
config = {}

def init(guild_id, newmin):
	config[guild_id] = {
		'min_votes': newmin
	}

def on_message(message):
	user_id = message.author.id
	is_bot = message.author.bot
	if is_bot is False or is_bot is None:
		if message.guild.id not in user_activity_map:
			user_activity_map[message.guild.id] = {}
		user_activity_map[message.guild.id][user_id] = time.time()

def get_min_voters(guild_id):
	print(user_activity_map)
	num_active = 0
	if guild_id in user_activity_map:
		print('guild id in map')
		for timestamp in user_activity_map[guild_id].values():
			date = datetime.fromtimestamp(timestamp)
			time_between = datetime.now() - date
			if time_between.seconds < 1800:
				num_active += 1
	min_votes = math.ceil(num_active / 3)
	if min_votes < config[guild_id]['min_votes']:
		min_votes = config[guild_id]['min_votes']
	return min_votes